# ungit-docker
This is a simple image for deploying _[Ungit](https://github.com/FredrikNoren/ungit)_ as a self-hosted web app.

## Approach

Since _Ungit_ is a _Node_ application, which also runs _Git_ commands in the backend, the following approach has been used:

  1. Start with an _Alpine_-based _Nodejs_ image
  1. Install _Git_, _GPG_ & _GPG-Agent_ from using _Alpine_'s built-in package manager (_apk_)
  1. Install _Ungit_ using _NPM_
  1. Expose _Ungit_'s operating port (`8448`)
  1. Switch to the working directory - assumed to be `/var/local/ungit/`

You can build it yourself if you wish. Here's the _Dockerfile_
```
  FROM node:20.5.1-alpine3.18
  RUN apk add --no-cache git gpg gpg-agent && rm -vrf /var/cache/apk/*
  RUN  npm install ungit@latest -g && npm cache clean --force
  EXPOSE 8448
  WORKDIR /var/local/ungit
```

_Ungit_'s launch command hasn't been baked into the image to allow tweaking by the user.

Ideally, the user can launch a _VS Code Server_ image, running as the same user, and use the same directory where the code rests to have an IDE + _Git_ client. You can go one step further by having an instance of _Caddy_ using the code repo as the HTML directory, also running as the same user.


## Deployment

To deploy the container as a stack, the following requirements must be met:

  1. The user who owns the code repositories must be running the container.
  1. The home directory of the user must be mapped to `/home/node/` in the container. The environment variable `HOME` must be overriden with this path, since the running user may not exist inside the container.
  1. _Ungit_'s bind IP must be set to `0.0.0.0` as part of the launch command to ensure that it can be accessed from outside the container.

Here's a sample _Docker Compose_ file

```yaml
version: '3'
name: "ungit"


services:

  main:

    container_name: "Ungit"
    restart: "unless-stopped"

    deploy:
      resources:
        limits:
          cpus: "0.25"
          memory: "256m"

    user: "${UID}:${GID}"

    healthcheck:
      test: "/usr/bin/wget --no-verbose --quiet --tries=1 --spider http://localhost:8448 || exit 1"
      interval: "60s"
      timeout: "10s"
      start_period: "20s"
      retries: 3

    networks:
      net_app:
        ipv4_address: "172.24.70.2"
    hostname: "ungit"
    extra_hosts:
      - "dockerhost:172.24.70.1"
#   ports:
#     - target: 8448
#       host_ip: "0.0.0.0"
#       published: 8448
#       protocol: "tcp"
#       mode: "host"

    volumes:
      - type: "bind"
        source: "/etc/localtime"
        target: "/etc/localtime"
        read_only: true
      - type: "bind"
        source: "/home/username"
        target: "/home/node"
        read_only: false
      - type: "bind"
        source: "/var/local/CodeRepo/Data"
        target: "/var/local/ungit"
        read_only: false

    environment:
      TZ: "Europe/London"
      HOME: "/home/node"

    image: "viharm/ungit:1.1.0_node-20.5.1_git-2.40.1_ungit-1.5.24"

    command:
      - "ungit"
      - "--logLevel=warn" # "none" < "error" < "warn" < "info" < "verbose" < "debug" < "silly"
      - "--ungitBindIp=0.0.0.0"
      - "--forcedLaunchPath=\"/var/local/ungit\""
      - "--no-launchBrowser"
      - "--no-bugTracking"
      - "--no-autoFetch"
      - "--tabSize=2"


networks:
  net_app:
    name: "Ungit_net"
    driver: "bridge"
    ipam:
      driver: "default"
      config:
        - subnet: "172.24.70.0/24"
```

Then serve this via a reverse proxy or expose the ports (uncomment above) for direct use. Remember to apply the appropriate authentication. I use _Authelia_.


## References

1) https://nodejs.org/en/docs/guides/nodejs-docker-webapp
2) https://dockerlabs.collabnix.com/beginners/dockerfile/lab1_dockerfile_git.html
3) https://stackoverflow.com/a/49119046
4) https://coder-coder.com/npm-clear-cache/
5) https://github.com/FredrikNoren/ungit/blob/master/source/config.js
6) https://github.com/nodejs/docker-node/blob/main/docs/BestPractices.md#non-root-user
