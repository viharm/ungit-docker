  # v1.1.0
  # Modified 29-Oct-2023
  FROM node:20.9.0-alpine3.18
  RUN apk add --no-cache git gpg gpg-agent && rm -vrf /var/cache/apk/*
  RUN  npm install ungit@1.5.24 -g && npm cache clean --force
  EXPOSE 8448
  WORKDIR /var/local/ungit
